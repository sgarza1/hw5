
import Tkinter as tk
import time
from math import sqrt, atan, cos, sin
import numpy as np
import argparse
import cv2
import random



global width, height, windowsize
width=1400
height=width/2
windowsize=500

class MotionCap(object):
    def __init__(self):
        #global cframe, roiPts, inputMode,scale
        self.cframe = None
        self.roipts1 = []
        self.roipts2 = []
        self.inputMode = False
        self.vel1= 0
        self.vel2 = 0
        self.velscale = 15
        self.thresh = 3
        self.release = 0 
        print 'initialized'

                  # construct the argument parse and parse the arguments
        self.ap = argparse.ArgumentParser()
        self.ap.add_argument("-v", "--video",
            help = "path to the (optional) video file")
        self.args = vars(self.ap.parse_args())

        # grab the reference to the current frame, list of ROI
        # points and whether or not it is ROI selection mode
        #global cframe, roiPts, inputMode

        # if the video path was not supplied, grab the reference to the
        # camera
        if not self.args.get("video", False):
            self.camera = cv2.VideoCapture(0)

        # otherwise, load the video
        else:
            self.camera = cv2.VideoCapture(args["video"])

        # setup the mouse callback
        cv2.namedWindow("cframe")
        cv2.setMouseCallback("cframe", self.selectROI1)

        



        # initialize the termination criteria for cam shift, indicating
        # a maximum of ten iterations or movement by a least one pixel
        # along with the bounding box of the ROI
        self.termination = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)
        self.ROIbox1 = None
        self.ROIbox2 = None
        self.oldpt1 = 0
        self.oldpt2 = 0

    def selectROI1(self,event, x, y, flags, param):
        # grab the reference to the current frame, list of ROI
        # points and whether or not it is ROI selection mode
        
        
        # if we are in ROI selection mode, the mouse was clicked,
        # and we do not already have four points, then update the
        # list of ROI points with the (x, y) location of the click
        # and draw the circle
        if self.inputMode and event == cv2.EVENT_LBUTTONDOWN and len(self.roipts1) < 4:
            self.roipts1.append((x, y))
            cv2.circle(self.cframe, (x, y), 4, (0, 255, 0), 2)
            cv2.imshow("cframe", self.cframe)
            

        if len(self.roipts1) == 4:
            cv2.setMouseCallback("cframe", self.selectROI2)

    def selectROI2(self,event,x,y,flags,param):

        if self.inputMode and event == cv2.EVENT_LBUTTONDOWN and  len(self.roipts1) == 4 and len(self.roipts2) < 4:
            self.roipts2.append((x, y))
            cv2.circle(self.cframe, (x, y), 4, (0, 0, 255), 2)
            cv2.imshow("cframe", self.cframe)
            


            if len(self.roipts2)==4:
                self.release = 1
              


    def main(self):
        
        
        # keep looping over the frames
        #while True:
        # grab the current frame
        (self.grabbed, self.cframe) = self.camera.read()

        # check to see if we have reached the end of the
        # video
        # if not self.grabbed:
        #     break

        # if the see if the ROI has been computed
        if self.ROIbox1 is not None and self.ROIbox2 is not None:
            # convert the current frame to the HSV color space
            # and perform mean shift
            hsv = cv2.cvtColor(self.cframe, cv2.COLOR_BGR2HSV)
            backProj1 = cv2.calcBackProject([hsv], [0], self.roiHist1, [0, 180], 1)
            backProj2 = cv2.calcBackProject([hsv], [0], self.roiHist2, [0, 180], 1)

            # apply cam shift to the back projection, convert the
            # points to a bounding box, and then draw them
            (self.r1, self.ROIbox1) = cv2.CamShift(backProj1, self.ROIbox1, self.termination)
            (self.r2, self.ROIbox2) = cv2.CamShift(backProj2, self.ROIbox2, self.termination)

            self.pts1 = np.int0(cv2.cv.BoxPoints(self.r1))
            self.pts2 = np.int0(cv2.cv.BoxPoints(self.r2))
            # print (self.pts1[1][0]+self.pts1[3][0])/2, (self.pts1[1][1]+self.pts1[3][1])/2
            # print  (self.pts2[1][0]+self.pts2[3][0])/2, (self.pts2[1][1]+self.pts2[3][1])/2

            cv2.polylines(self.cframe, [self.pts1], True, (0, 255, 0), 2)
            cv2.polylines(self.cframe, [self.pts2], True, (0, 0, 255), 2)

            cv2.circle(self.cframe, ((self.pts1[1][0]+self.pts1[3][0])/2, ((self.pts1[1][1]+self.pts1[3][1])/2)), 4, (0, 255, 0), 2)
            cv2.circle(self.cframe, ((self.pts2[1][0]+self.pts2[3][0])/2, ((self.pts2[1][1]+self.pts2[3][1])/2)), 4, (0, 0, 255), 2)

            
            if (self.pts1[1][1]+self.pts1[3][1])/2-self.oldpt1>self.thresh:
                self.vel1= -1 *self.velscale

            elif (self.pts1[1][1]+self.pts1[3][1])/2-self.oldpt1<-self.thresh:
                self.vel1= 1*self.velscale
            else:
                self.vel1= 0*self.velscale

            self.oldpt1 = (self.pts1[1][1]+self.pts1[3][1])/2

            if (self.pts2[1][1]+self.pts2[3][1])/2-self.oldpt2>self.thresh:
                self.vel2= -1 *self.velscale

            elif (self.pts2[1][1]+self.pts2[3][1])/2-self.oldpt2<-self.thresh:
                self.vel2= 1*self.velscale
            else:
                self.vel2= 0*self.velscale

            self.oldpt2 = (self.pts2[1][1]+self.pts2[3][1])/2


            
            
           

        # show the frame and record if the user presses a key
        cv2.imshow("cframe", self.cframe)
        key = cv2.waitKey(1) & 0xFF

        # handle if the 'i' key is pressed, then go into ROI
        # selection mode
        if key == ord("i") and len(self.roipts1) < 4:
            # indicate that we are in input mode and clone the
            # frame
            self.inputMode = True
            orig = self.cframe.copy()

            # keep looping until 4 reference ROI points have
            # been selected; press any key to exit ROI selction
            # mode once 4 points have been selected
            while len(self.roipts1) < 4 or len(self.roipts2) < 4:
                cv2.imshow("cframe", self.cframe)
                cv2.waitKey(0)

            # determine the top-left and bottom-right points
            self.roipts1 = np.array(self.roipts1)
            self.s1 = self.roipts1.sum(axis = 1)
            self.tl1 = self.roipts1[np.argmin(self.s1)]
            self.br1 = self.roipts1[np.argmax(self.s1)]

            self.roipts2 = np.array(self.roipts2)
            self.s2 = self.roipts2.sum(axis = 1)
            self.tl2 = self.roipts2[np.argmin(self.s2)]
            self.br2 = self.roipts2[np.argmax(self.s2)]



            # grab the ROI for the bounding box and convert it
            # to the HSV color space
            self.roi1 = orig[self.tl1[1]:self.br1[1], self.tl1[0]:self.br1[0]]
            self.roi1 = cv2.cvtColor(self.roi1, cv2.COLOR_BGR2HSV)
            #roi = cv2.cvtColor(roi, cv2.COLOR_BGR2LAB)


            # grab the ROI for the bounding box and convert it
            # to the HSV color space
            self.roi2 = orig[self.tl2[1]:self.br2[1], self.tl2[0]:self.br2[0]]
            self.roi2 = cv2.cvtColor(self.roi2, cv2.COLOR_BGR2HSV)
            #roi = cv2.cvtColor(roi, cv2.COLOR_BGR2LAB)

            # compute a HSV histogram for the ROI and store the
            # bounding box
            self.roiHist1 = cv2.calcHist([self.roi1], [0], None, [16], [0, 180])
            self.roiHist1 = cv2.normalize(self.roiHist1, self.roiHist1, 0, 255, cv2.NORM_MINMAX)
            self.ROIbox1 = (self.tl1[0], self.tl1[1], self.br1[0], self.br1[1])

        
            # compute a HSV histogram for the ROI and store the
            # bounding box
            self.roiHist2 = cv2.calcHist([self.roi2], [0], None, [16], [0, 180])
            self.roiHist2 = cv2.normalize(self.roiHist2, self.roiHist2, 0, 255, cv2.NORM_MINMAX)
            self.ROIbox2 = (self.tl2[0], self.tl2[1], self.br2[0], self.br2[1])



       



class Interface(tk.Frame):

    def __init__ (self,master=None):
        tk.Frame.__init__(self, master)

        global frame

        self.master.rowconfigure(0,weight=1)
        self.master.columnconfigure(0,weight=1)
        frame = tk.Canvas(master, width=width, height=height,background='black')
        frame.pack()

        self.welcometext=frame.create_text(width/2,height/3,text='Welcome to Pong!',font= 'Times 20', fill='yellow')
        self.directiontext=frame.create_text(width/2,height/2,text='Please Select a Game Type',fill='yellow',font= 'Times 20')
        self.option1text=frame.create_text(width/6,3*height/5,text='Standard',fill='yellow',font= 'Times 20')
        self.option2text=frame.create_text(5*width/6,3*height/5,text='MotionControled',fill='yellow',font= 'Times 20')

    def flashText(self,color):
        
        frame.delete(self.welcometext)
        self.welcometext=frame.create_text(width/2,height/3,text='Welcome to Pong!',font= 'Times 20', fill=color)
        self.UpdateFrame()
  
    def drawMotionControlDirections(self):
        self.welcometext=frame.create_text(width/2,height/5,text='Select two items for controllers',font= 'Times 20', fill='yellow')
        self.directiontext2=frame.create_text(width/2,height*.4,text='Press ''i'' to pause the webcam',fill='yellow',font= 'Times 20')
        self.directiontext3=frame.create_text(width/2,height*.6,text='Select four corners of each object',fill='yellow',font= 'Times 20')
        self.directiontext4=frame.create_text(width/2,height*.8,text='Aftwards Press Enter twice to continue',fill='yellow',font= 'Times 20')
        self.UpdateFrame()

    def clearMotionControlDirections(self):
        frame.delete(self.welcometext)
        frame.delete(self.directiontext2)
        frame.delete(self.directiontext3)
        frame.delete(self.directiontext4)



    def drawLayout(self):
        self.centerline=frame.create_line(width/2,0,width/2,height,fill='white')
        
        '''creates instruction text'''
        self.title=frame.create_text(width/2,height/2,text='Press Enter to Start a Round',font= 'Times 20', fill='yellow')
        self.text2=frame.create_text(width/4,50,text='Press W/S to move up/down',fill='yellow',font= 'Times 20')
        self.text3=frame.create_text(3*width/4,50,text='Press Up/Down to move up/down',fill='yellow',font= 'Times 20')
        
        self.quitbutton=frame.create_text(width*.02,height*.98,text='QUIT',fill='red',font= 'Times 20')
        self.resetbutton=frame.create_text(width*.98,height*.98,text='RESET',fill='red',font= 'Times 20')

        '''defines boundries'''
        self.leftbound=0
        self.rightbound=windowsize        
        self.topbound=0
        self.bottombound=windowsize

        '''creates score to be displayed'''
        self.scoretext_l=frame.create_text(width*.25,20,text=str(0),fill='white',font= 'Times 20')
        self.scoretext_r=frame.create_text(width*.75,20,text=str(0),fill='white',font= 'Times 20')

    def createScales(self):
        '''creates scales'''
        self.ballspeed=tk.Scale(root,from_=1, to=3, label='Ball Speed', resolution=0.5, orient= tk.HORIZONTAL)
        self.ballspeed.pack(side=tk.LEFT)
        self.paddlespeed=tk.Scale(root,from_=1, to=15,label='Paddle Speed', orient=tk.HORIZONTAL)
        self.paddlespeed.set(4)
        self.paddlespeed.pack(side=tk.LEFT)

        
    def ClearScore(self):
        frame.delete(self.scoretext_l)
        frame.delete(self.scoretext_r)

    def UpdateScore(self,score):
        self.scoretext_l=frame.create_text(width*.25,20,text=str(score[0]),fill='white')
        self.scoretext_r=frame.create_text(width*.75,20,text=str(score[1]),fill='white')

    def UpdateFrame(self):
        frame.update()

    def clearIntro(self):
        frame.delete(self.welcometext)
        frame.delete(self.directiontext)
        frame.delete(self.option1text)
        frame.delete(self.option2text)

    def cleartitle(self,event):
         
        frame.delete(self.title)
        frame.delete(self.text2)
        frame.delete(self.text3)
        pong.start=1
        pong.startround()

    def drawLines(self,gamepiece):
        if gamepiece.name == 'Left':

            self.p1liney1=frame.create_line(gamepiece.x1,gamepiece.y1,gamepiece.x1,gamepiece.y2,fill='green')
            self.p1liney2=frame.create_line(gamepiece.x2,gamepiece.y1,gamepiece.x2,gamepiece.y2,fill='green')
            self.p1linex1=frame.create_line(gamepiece.x1,gamepiece.y1,gamepiece.x2,gamepiece.y1,fill='green')
            self.p1linex2=frame.create_line(gamepiece.x1,gamepiece.y2,gamepiece.x2,gamepiece.y2,fill='green')

        if gamepiece.name == 'Right':

            self.p2liney1=frame.create_line(gamepiece.x1,gamepiece.y1,gamepiece.x1,gamepiece.y2,fill='green')
            self.p2liney2=frame.create_line(gamepiece.x2,gamepiece.y1,gamepiece.x2,gamepiece.y2,fill='green')
            self.p2linex1=frame.create_line(gamepiece.x1,gamepiece.y1,gamepiece.x2,gamepiece.y1,fill='green')
            self.p2linex2=frame.create_line(gamepiece.x1,gamepiece.y2,gamepiece.x2,gamepiece.y2,fill='green')



    def deleteLines(self,gamepiece):
        if gamepiece.name == 'Left':

            frame.delete(self.p1linex1)
            frame.delete(self.p1liney1)
            frame.delete(self.p1linex2)
            frame.delete(self.p1liney2)

        if gamepiece.name == 'Right':

            frame.delete(self.p2linex1)
            frame.delete(self.p2liney1)
            frame.delete(self.p2linex2)
            frame.delete(self.p2liney2)

    def CreateImage(self,gamepiece):

        if gamepiece.type == 'monster':
            self.monsterphoto = tk.PhotoImage(file = 'monster.gif')
            self.monsterimage = frame.create_image(gamepiece.centerx,gamepiece.centery,image=self.monsterphoto)

        if gamepiece.type == 'ball':
            self.ballphoto = tk.PhotoImage(file = 'ball.gif')
            self.ballimage = frame.create_image(gamepiece.centerx,gamepiece.centery,image=self.ballphoto)

        if gamepiece.type == 'portal':
            self.portalphoto = tk.PhotoImage(file = 'portal.gif')
            self.portalimage = frame.create_image(gamepiece.centerx,gamepiece.centery,image=self.portalphoto)

        if gamepiece.type == 'cloud':
            self.cloudphoto = tk.PhotoImage(file = 'jokercloud.gif')
            self.cloudimage = frame.create_image(gamepiece.centerx,gamepiece.centery,image=self.cloudphoto)

        if gamepiece.type == 'paddle':
            if gamepiece.name == 'Left':
                if gamepiece.parameter == 0:
                    
                    self.paddle1photo = tk.PhotoImage(file = 'benitoface.gif')
                else:
                    
                    self.paddle1photo = tk.PhotoImage(file = 'carlos.gif')

                self.paddle1image = frame.create_image(gamepiece.centerx,gamepiece.centery,image=self.paddle1photo)


            if gamepiece.name == 'Right':
                if gamepiece.parameter == 0:
                    self.paddle2photo = tk.PhotoImage(file = 'luisface.gif')
                   
                else:
                
                    self.paddle2photo = tk.PhotoImage(file = 'jackface.gif')

                self.paddle2image = frame.create_image(gamepiece.centerx,gamepiece.centery,image=self.paddle2photo)
    

    def DeleteImage(self,image):
        frame.delete(image)


    def MoveImage(self,image,dx,dy):
        frame.move(image,dx,dy)

    def CreateWallImage (self,coordinates):

        self.breakoutphoto = tk.PhotoImage(file = 'joseface.gif')
        
        self.breakoutimage1 = frame.create_image(coordinates[0][0],coordinates[0][1],image = self.breakoutphoto)
        self.breakoutimage2 = frame.create_image(coordinates[1][0],coordinates[1][1],image = self.breakoutphoto)
        self.breakoutimage3 = frame.create_image(coordinates[2][0],coordinates[2][1],image = self.breakoutphoto)
        self.breakoutimage4 = frame.create_image(coordinates[3][0],coordinates[3][1],image = self.breakoutphoto)
        self.breakoutimage5 = frame.create_image(coordinates[4][0],coordinates[4][1],image = self.breakoutphoto)

    def CreatePortalImages (self,coordinates):
        self.portalphoto = tk.PhotoImage(file = 'portal.gif')
        
        self.portalimage1 = frame.create_image(coordinates[0][0],coordinates[0][1],image = self.portalphoto)
        self.portalimage2 = frame.create_image(coordinates[1][0],coordinates[1][1],image = self.portalphoto)
        self.portalimage3 = frame.create_image(coordinates[2][0],coordinates[2][1],image = self.portalphoto)
        self.portalimage4 = frame.create_image(coordinates[3][0],coordinates[3][1],image = self.portalphoto)

    def DeletePortalImages(self):

        frame.delete(self.portalimage1)
        frame.delete(self.portalimage2)
        frame.delete(self.portalimage3)
        frame.delete(self.portalimage4)
        

    def DeleteWallImage(self):

        frame.delete(self.breakoutimage1)
        frame.delete(self.breakoutimage2)
        frame.delete(self.breakoutimage3)
        frame.delete(self.breakoutimage4)
        frame.delete(self.breakoutimage5)



class Ball(object):
    def __init__(self):
        """
        Create a pong ball with the given inital position. 
        """

        self.type = 'ball'

        self.centerx = width/2
        self.centery = height/2

        xsize=15
        ysize=20

        self.radius= sqrt(xsize^2 + ysize^2)

        self.x1=self.centerx-xsize
        self.x2=self.centerx+xsize*.9
        self.y1=self.centery-ysize*.7
        self.y2=self.centery+ysize*.7

        self.ballspeedvar=gui.ballspeed.get()
        self.velocity = (self.ballspeedvar*random.choice((-3,3)),self.ballspeedvar*random.choice((-3,3)))
       

    def reflect(self,parameter):
        """
        If parameter = 0. Reflect in the HORIZONTAL direction
        If parameter = 1. Reflect in the VERTICAL direction
        """
        if parameter==0:
            self.velocity=(-self.velocity[0],self.velocity[1])
        if parameter==1:
            self.velocity=(self.velocity[0],-self.velocity[1])
        
            
    def UpdatePosition(self,dx,dy):
        """
        Increment Monster Position
        """
       
        
        self.x1+=dx
        self.x2+=dx
        self.centerx+=dx

        self.y1+=dy
        self.y2+=dy
        self.centery+=dy



class Monster(Ball):

    def __init__(self):

    	self.type = 'monster'
        xsize=40
        ysize=75

        
        self.velocity = (gui.ballspeed.get()*random.choice((-1,1)),gui.ballspeed.get()*random.choice((-1,1)))

        self.centerx = width/2
        self.centery = height/4

        self.x1=self.centerx-xsize
        self.x2=self.centerx+xsize*.9
        self.y1=self.centery-ysize
        self.y2=self.centery+ysize*.7

        self.radius = (self.x2 - self.x1)/2.0 #used as a rought approximation for paddle rebounds

        self.state = 1
             

class Paddle(object):

    def __init__(self,name,parameter=0):

        self.type = 'paddle'
        self.parameter = parameter
        self.name=name
        self.centery = height/2
        self.speed = 0

        xsize=25 #default paddle sizes #25
        ysize=62 #62

        if(self.name=='Left'):
          
            self.centerx = width*.01

            if parameter == 1:
                xsize=10 #defualt is 8... may have mixed up sides
                ysize=85 #default is 85


         
                

        if self.name == 'Right':
         
            self.centerx = width * .99

            if parameter == 1:
                xsize=35
                ysize=80
           
               

        frame.focus_set()

        
        'create a paddle with a given initial condition'
        

        self.x1=self.centerx-xsize
        self.x2=self.centerx+xsize*.9
        self.y1=self.centery-ysize*.7
        self.y2=self.centery+ysize*.7

        

       
    print 'gui on'

    
        

    def setpaddlevelocity(self,velocity):
    	
        self.paddlespeedvar=gui.paddlespeed.get()
        self.speed=velocity*(self.paddlespeedvar)
       
        
    def UpdatePosition(self,speed):
        self.y1 -=speed
        self.y2 -=speed


class Portal(object):
    def __init__(self,centerx,centery):

        ''' Initiates a portal. Creates model data including location and size.
            
        '''
        
        self.type = 'portal'
        xsize = 40
        ysize = 40
        self.centerx = centerx
        self.centery = centery

        self.x1=centerx-xsize
        self.x2=centerx+xsize

        self.y1=centery-ysize
        self.y2=centery+ysize

        self.state = 1
        

class Cloud(object):
    
    def __init__(self,centerx,centery):

    	self.type = 'cloud'
        
        xsize = 85 #default of 85
        ysize = 30 #default of 30
        self.centerx = centerx
        self.centery = centery

        self.x1=centerx-xsize
        self.x2=centerx+xsize*.95

        self.y1=centery-ysize
        self.y2=centery+ysize*.9

        self.state = 1


class Obstacle(object):

    def __init__(self,x1,counter):
        """
        Create a wall element. Assigns size and position information.  
        Sets initial state to 1. Meaning the obstacle is currently active.
        """
        self.type = 'breakout'

        sizex = 50 #default of 50
        sizey = 75 #default of 75
        self.x1 = x1
        self.x2 = self.x1+sizex
        self.centerx=(self.x1+self.x2)/2-5

        self.y1 = counter*60+45
        self.y2 = self.y1+sizey

      	self.centery = (self.y1 + self.y2)/2
    
        self.state=1

class Gravity(object):

    def __init__(self):
        self.state = 1
        self.acc = 9.8/50
        self.direction = 1 
               

class Pong(object):
    def __init__(self):
        frame.bind('<Button-1>',self.onmouse)
        self.welcometextcolors = ['yellow','red','blue','green']
        self.colorindex = 0
        self.flashstate = 1

    def StartGame(self):

        gui.drawLayout()
        gui.createScales()


        self.roundcount=0
        self.start=0
        self.mousex=0
        self.mousey=0

        self.score=[0,0]
        self.bouncenum=0
        self.state=0
        

        self.ball = Ball()
        self.paddle1=Paddle('Left',parameter=1)
        self.paddle2=Paddle('Right',parameter=1)
        self.cloud = Cloud (width*.3,height*.3)
        self.monster=Monster()
        self.createPortals()
        self.createWall()

        self.gravity = Gravity()
        self.acc = 9.8
        
        

        gui.CreateImage(self.monster)
        gui.CreateImage(self.ball)
        gui.CreateImage(self.cloud)

        gui.CreatePortalImages(self.getPortalCoordinates())
        gui.CreateWallImage(self.getWallCoordinates())

        gui.CreateImage(self.paddle1)
        gui.drawLines(self.paddle1)

        gui.CreateImage(self.paddle2)
        gui.drawLines(self.paddle2)

        self.assignBindings(self.paddle1)
        self.assignBindings(self.paddle2)
        
        frame.bind("<Return>",gui.cleartitle)
        

        if self.cap == 1:

            self.motion = MotionCap()
            self.ball.velocity = (self.ball.velocity[0]*2.5,self.ball.velocity[1]*2.5)

            while self.motion.release == 0:
                self.motion.main()

        if self.motion.release == 1:
            print 'clearing'
            gui.clearMotionControlDirections()

    def assignBindings(self,gamepiece = 0):
        if gamepiece.name == 'Left':

            frame.bind("<KeyPress-w>", lambda _: self.paddle1.setpaddlevelocity(1))
            frame.bind("<KeyRelease-w>",lambda _: self.paddle1.setpaddlevelocity(0))
            frame.bind("<KeyPress-s>", lambda _: self.paddle1.setpaddlevelocity(-1))
            frame.bind("<KeyRelease-s>",lambda _: self.paddle1.setpaddlevelocity(0))


        if gamepiece.name == 'Right':
            frame.bind("<KeyPress-Down>", lambda _:self.paddle2.setpaddlevelocity(-1))
            frame.bind("<KeyPress-Up>", lambda _:self.paddle2.setpaddlevelocity(1)) #needed to use tab and shift. 
            frame.bind("<KeyRelease-Down>",lambda _: self.paddle2.setpaddlevelocity(0))
            frame.bind("<KeyRelease-Up>", lambda _:self.paddle2.setpaddlevelocity(0)) #needed to use tab and shift. 
   
    def createWall(self):
        
        x1=random.randrange(width*.25,width*.75)   

        self.obstacle1=Obstacle(x1,0)
        self.obstacle2=Obstacle(x1,2)
        self.obstacle3=Obstacle(x1,4)
        self.obstacle4=Obstacle(x1,6)
        self.obstacle5=Obstacle(x1,8)


    def getWallCoordinates(self):
        return          [[self.obstacle1.centerx,self.obstacle1.centery],[self.obstacle2.centerx,self.obstacle2.centery],\
                        [self.obstacle3.centerx,self.obstacle3.centery],[self.obstacle4.centerx,self.obstacle4.centery],\
                        [self.obstacle5.centerx,self.obstacle5.centery]]


    def createPortals(self):
        self.portal1=Portal(width*.125,40)
        self.portal2=Portal(width*.875,40)
        self.portal3=Portal(width*.125,height-35)
        self.portal4=Portal(width*.875,height-35)

    def getPortalCoordinates(self):
        return          [[self.portal1.centerx,self.portal1.centery],[self.portal2.centerx,self.portal2.centery],\
                        [self.portal3.centerx,self.portal3.centery],[self.portal4.centerx,self.portal4.centery]]
                        
    def allowPaddleMovement(self,paddle):
        if paddle.speed < 0:
            if paddle.y2 <= height:
                return 1

        if paddle.speed > 0:
            if paddle.y1 >= 0:
                return 1

    def hitHorizontalPlane(self,projectile,barrier):
        if projectile.velocity[0]<0:
            distancex=abs(projectile.centerx-barrier.x2)
        else:
            distancex=abs(projectile.centerx-barrier.x1)

        if abs(projectile.velocity[0])<.00001:
            projectile.velocity = (projectile.velocity[1],projectile.velocity[0]+1)
        theta=atan(projectile.velocity[1]/projectile.velocity[0])
        distance=distancex/abs(cos(theta))


        if barrier.type == 'paddle':
            buff = 2
        else:
            buff = 1
        if distance <= projectile.radius*buff and projectile.centery<=barrier.y2 and projectile.centery>=barrier.y1:
            return 1

    def hitVerticalPlane(self,projectile,barrier):
        if projectile.velocity[1]<0:
            distancey=abs(projectile.centery-barrier.y2)
        else:
            distancey=abs(projectile.centerx-barrier.y1)

        theta=atan(projectile.velocity[1]/projectile.velocity[0]+.0001)*3.14159/180
        distance=distancey/abs(sin(theta))

        if distance <= projectile.radius and projectile.centerx<=barrier.x2 and projectile.centerx>=barrier.x1:
                
                return 1

    def spiral(self):
        xold = 0
        yold = 0 
        arm = 60

        for theta in range(360):
            
            xnew = cos(theta*3.14159/180) * arm
            ynew = sin(theta*3.14159/180) * arm

            dx = xnew-xold
            dy = ynew - yold

            self.ball.UpdatePosition(dx,dy)
            gui.MoveImage(gui.ballimage,dx,dy)

            xold = xnew
            yold = ynew
            arm+=1

            gui.UpdateFrame()

    def onmouse(self,event):
        self.mousex=(event.x)
        self.mousey=(event.y)
        
        if(self.mousex>=0 and self.mousex<=width*.04 and self.mousey>=height*.97):
            root.quit()
            
        elif(self.mousex>=.96*width and self.mousex<=width and self.mousey>=height*.97):
            self.resetscore(1)

        elif(self.mousex>=.96*width and self.mousex<=width and self.mousey>=height*.97):
            self.resetscore(1)

        while self.flashstate == 1:

            gui.flashText(self.welcometextcolors[self.colorindex%4])
            self.colorindex+=1

            if(self.mousex>=width/7.1 and self.mousex<=width/4.9 and self.mousey>=height*.55 and self.mousey<=height*.65):
                self.flashstate = 0
                self.cap = 0
                gui.clearIntro()
                self.StartGame()

            elif(self.mousex>=5*width/6.5 and self.mousex<=5*width/5.5 and self.mousey>=height*.55 and self.mousey<=height*.65):
                self.flashstate = 0
                self.cap = 1
                gui.clearIntro()
                gui.drawMotionControlDirections()
                time.sleep(5)
                self.StartGame()
            time.sleep(.5)

            
    def edgecheck(self,projectile):

        if(projectile.x2>=width) or projectile.x1<=0:
            
            if projectile.type != 'ball':
                projectile.reflect(0)

            else:
                    if projectile.x2 >= width:
                        self.score[0]+=1
                        print "scored right"
                        

                    if (projectile.x1<=0):
                        self.score[1]+=1
                        print "Scored left"
                        
                    self.resetballpaddle()
                    self.resetobstacles()
                    




        if projectile.y2 >= height or projectile.y1 <=0:
            projectile.reflect(1)

    def portalcheck(self,portal):
        if (self.ball.x1>=portal.x1) and (self.ball.x2<=portal.x2) and (self.ball.y1>=portal.y1) and (self.ball.y2<=portal.y2):
                    
                if portal.centerx==width*.125 and portal.centery==40: 
                    dx = width/2
                    dy = height/2          

                if portal.centerx==width*.875 and portal.centery==40: 
                    dx = -width/2
                    dy = height/3      
                    
                if portal.centerx==width*.125 and portal.centery==height-35: 
                    dx=width/4
                    dy=-height/1.5
                    
                if portal.centerx==width*.875 and portal.centery==height-35: 
                    dx=-width/2
                    dy=-height/4
                    
                self.ball.UpdatePosition(dx,dy)
                gui.MoveImage(gui.ballimage,dx,dy) #Here I want to specifically move the ball and only the ball.
                self.ball.reflect(parameter=random.choice((0,1)))         

    def breakoutcheck(self,breakout,image):
       
        if breakout.state == 1:
            if self.hitHorizontalPlane(self.ball,breakout) == 1:
                gui.DeleteImage(image)
                breakout.state=0
                self.ball.reflect(0)
                

            elif self.hitVerticalPlane(self.ball,breakout) == 1:
                gui.DeleteImage(image)
                breakout.state=0
                self.ball.reflect(1)

            elif (self.ball.x1>=breakout.x1) and (self.ball.x2<=breakout.x2) and (self.ball.y1>=breakout.y1) and (self.ball.y2<=breakout.y2):
                gui.DeleteImage(image)
                breakout.state=0
                self.ball.reflect(1)

    def paddlecheck(self,projectile,paddle):
        if self.hitHorizontalPlane(projectile,paddle) == 1:
                projectile.reflect(0)
                if projectile.type == 'ball':
                    self.bouncenum += 1

        elif self.hitVerticalPlane(projectile,paddle) == 1:
                projectile.reflect(1)
                if projectile.type == 'ball':
                    self.bouncenum += 1
        elif (projectile.x1>=paddle.x1) and (projectile.x2<=paddle.x2) and (projectile.y1>=paddle.y1) and (projectile.y2<=paddle.y2):
                projectile.reflect(0)

    def setportalstates(self,parameter = 1):
        self.portal1.state = parameter
        self.portal2.state = parameter
        self.portal3.state = parameter
        self.portal4.state = parameter

    def setobstaclestates(self,parameter = 1):
        self.obstacle1.state = parameter
        self.obstacle2.state = parameter
        self.obstacle3.state = parameter
        self.obstacle4.state = parameter
        self.obstacle5.state = parameter

    def cloudcheck(self):

        if self.hitHorizontalPlane(self.ball,self.cloud) == 1:
            self.spiral()
            

        elif self.hitVerticalPlane(self.ball,self.cloud) == 1:
            self.spiral()
            

        elif (self.ball.x1>=self.cloud.x1) and (self.ball.x2<=self.cloud.x2) and (self.ball.y1>=self.cloud.y1) and (self.ball.y2<=self.cloud.y2):
            self.spiral()

    def monstercheck(self):

        if self.hitHorizontalPlane(self.ball,self.monster) == 1:
            self.ball.reflect(0)

        elif self.hitVerticalPlane(self.ball,self.monster) == 1:
            self.ball.reflect(1)

        elif (self.ball.x1>=self.monster.x1) and (self.ball.x2<=self.monster.x2) and (self.ball.y1>=self.monster.y1) and (self.ball.y2<=self.monster.y2):
            self.ball.reflect(1)

    def generaterandomobstacles(self):

        self.setportalstates(parameter=random.choice((0,1))) 
        self.setobstaclestates(parameter=random.choice((0,1))) 
        self.monster.state = random.choice((0,1))
        self.cloud.state = random.choice((0,0,0,1))
        self.gravity.state = random.choice((0,0,1))

        self.resetobstacles()

        
    def startround(self):
       
        
        time0 = 0
        time00 = 0

        while(True):

            while True:
                
                if time.clock() - time00 >= 2:
                    self.generaterandomobstacles()
                    time00 = time.clock()
   

                if(self.bouncenum%2==0) and (self.bouncenum != 0) and self.state==1:

                    counter=(counter+1)%5
                    self.ball.velocity=(self.ball.velocity[0]*1.4,self.ball.velocity[1]*1.4)
                    self.state=0

                timediff = time.clock()-time0
                time0 = time.clock()

                if self.cap == 1:
                        self.motion.main()
                        self.paddle1.speed = self.motion.vel1
                        self.paddle2.speed = self.motion.vel2
                       

        
                if self.allowPaddleMovement(self.paddle1) == 1:

                    self.paddle1.UpdatePosition(self.paddle1.speed)
                    gui.MoveImage(gui.paddle1image,0,-self.paddle1.speed)
                    gui.deleteLines(self.paddle1)
                    gui.drawLines(self.paddle1)

                if self.allowPaddleMovement(self.paddle2) == 1:

                    self.paddle2.UpdatePosition(self.paddle2.speed)
                    gui.MoveImage(gui.paddle2image,0,-self.paddle2.speed)
                    gui.deleteLines(self.paddle2)
                    gui.drawLines(self.paddle2)

                    
                 
                    
                                    
                self.paddlecheck(self.ball,self.paddle1)
                self.paddlecheck(self.ball,self.paddle2)
                self.edgecheck(self.ball)

                if self.monster.state == 1:
                    self.paddlecheck(self.monster,self.paddle1)
                    self.paddlecheck(self.monster,self.paddle2)
                    self.edgecheck(self.monster) 
                    self.monstercheck()
                    self.monster.UpdatePosition(self.monster.velocity[0],self.monster.velocity[1])
                    gui.MoveImage(gui.monsterimage,self.monster.velocity[0],self.monster.velocity[1])

                
                if self.obstacle1.state == 1:
                    self.breakoutcheck(self.obstacle1,gui.breakoutimage1)
                    self.breakoutcheck(self.obstacle2,gui.breakoutimage2)
                    self.breakoutcheck(self.obstacle3,gui.breakoutimage3)
                    self.breakoutcheck(self.obstacle4,gui.breakoutimage4)
                    self.breakoutcheck(self.obstacle5,gui.breakoutimage5)

                if self.portal1.state == 1:
                    self.portalcheck(self.portal1)
                    self.portalcheck(self.portal2)
                    self.portalcheck(self.portal3)
                    self.portalcheck(self.portal4)

                if self.cloud.state == 1:
                    self.cloudcheck()

                
                if self.gravity.state == 1:

                    self.ball.velocity = (self.ball.velocity[0] ,self.ball.velocity[1] + self.gravity.direction*self.gravity.acc)



                self.ball.UpdatePosition(self.ball.velocity[0],self.ball.velocity[1])
                gui.MoveImage(gui.ballimage,self.ball.velocity[0],self.ball.velocity[1])

                

            	time.sleep(.01)
                
                gui.UpdateFrame()


            	break
    def resetscore(self,default=0):
        if default==1:          
            
            gui.ClearScore()
            self.score=[0,0]
            gui.UpdateScore([0,0])
            self.start=0
            

    def resetballpaddle(self):

        gui.ClearScore()
        gui.UpdateScore((self.score[0],self.score[1]))
        self.roundcount+=1

        self.start=0
        self.bouncenum=0

       

        gui.DeleteImage(gui.paddle1image)
        gui.deleteLines(self.paddle1)


        gui.DeleteImage(gui.paddle2image)
        gui.deleteLines(self.paddle2)

        self.paddle1=Paddle('Left',parameter=(self.roundcount%2))
        self.paddle2=Paddle('Right',parameter=(self.roundcount%2))

        gui.CreateImage(self.paddle1)
        gui.CreateImage(self.paddle2)

        gui.drawLines(self.paddle1)
        gui.drawLines(self.paddle2)


        gui.DeleteImage(gui.ballimage)
        self.ball=Ball()
        gui.CreateImage(self.ball)
        
        
     

        if self.cap == 1:
            self.ball.velocity = (self.ball.velocity[0]*2.5,self.ball.velocity[1]*2.5)

    def resetobstacles(self):

        if self.portal1.state == 1:
            gui.DeletePortalImages()
            self.createPortals()
            gui.CreatePortalImages(self.getPortalCoordinates())

        else:
            gui.DeletePortalImages()


        if self.cloud.state == 1:
            gui.DeleteImage(gui.cloudimage)
            self.cloud=Cloud(width*.5,height*.5)
            gui.CreateImage(self.cloud)
        else:
             gui.DeleteImage(gui.cloudimage)

        
        if self.monster.state == 1:
            gui.DeleteImage(gui.monsterimage)
            self.monster=Monster()
            gui.CreateImage(self.monster)
        else:
            gui.DeleteImage(gui.monsterimage)


        if self.obstacle1.state == 1:
            gui.DeleteWallImage()
            self.createWall()
            gui.CreateWallImage(self.getWallCoordinates())
        else:
            gui.DeleteWallImage()

        if self.gravity.state == 1:
            self.gravity.direction = random.choice((1,-1))

                
    

global root,gui
root=tk.Tk()




gui=Interface(master=root)
gui.master.title('Pong')


pong=Pong()

# motion.main()

gui.mainloop()
gui.destroy()
self.camera.release()
cv2.destroyAllWindows()