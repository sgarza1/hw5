
import Tkinter as tk
import time
from math import sqrt, atan, cos, sin
import numpy as np
import argparse
import cv2

import random



global width, height, windowsize
width=1400
height=width/2
windowsize=500

class MotionCap(object):
    def __init__(self):
        global cframe, roiPts, inputMode,scale
        cframe = None
        roiPts = []
        inputMode = False
        scale = 5

    def selectROI(self,event, x, y, flags, param):
        # grab the reference to the current frame, list of ROI
        # points and whether or not it is ROI selection mode
        print 'called'
        
       

        # if we are in ROI selection mode, the mouse was clicked,
        # and we do not already have four points, then update the
        # list of ROI points with the (x, y) location of the click
        # and draw the circle
        if inputMode and event == cv2.EVENT_LBUTTONDOWN and len(roiPts) < 4:
            roiPts.append((x, y))
            cv2.circle(cframe, (x, y), 4, (0, 255, 0), 2)
            cv2.imshow("cframe", cframe)

    def main(self):
            # construct the argument parse and parse the arguments
        ap = argparse.ArgumentParser()
        ap.add_argument("-v", "--video",
            help = "path to the (optional) video file")
        args = vars(ap.parse_args())

        # grab the reference to the current frame, list of ROI
        # points and whether or not it is ROI selection mode
        global cframe, roiPts, inputMode

        # if the video path was not supplied, grab the reference to the
        # camera
        if not args.get("video", False):
            camera = cv2.VideoCapture(0)

        # otherwise, load the video
        else:
            camera = cv2.VideoCapture(args["video"])

        # setup the mouse callback
        cv2.namedWindow("cframe")
        cv2.setMouseCallback("cframe", self.selectROI)

        # initialize the termination criteria for cam shift, indicating
        # a maximum of ten iterations or movement by a least one pixel
        # along with the bounding box of the ROI
        termination = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)
        roiBox = None
        oldPt = 0

        # keep looping over the frames
        while True:
            # grab the current frame
            (grabbed, cframe) = camera.read()

            # check to see if we have reached the end of the
            # video
            if not grabbed:
                break

            # if the see if the ROI has been computed
            if roiBox is not None:
                # convert the current frame to the HSV color space
                # and perform mean shift
                hsv = cv2.cvtColor(cframe, cv2.COLOR_BGR2HSV)
                backProj = cv2.calcBackProject([hsv], [0], roiHist, [0, 180], 1)

                # apply cam shift to the back projection, convert the
                # points to a bounding box, and then draw them
                (r, roiBox) = cv2.CamShift(backProj, roiBox, termination)
                pts = np.int0(cv2.cv.BoxPoints(r))
                cv2.polylines(cframe, [pts], True, (0, 255, 0), 2)
                # print pts 
                if pts[1][1]-oldPt>0:
                    self.vel = 1 *scale
                elif pts[1][1]-oldPt<0:
                    self.vel = -1*scale
                else:
                    self.vel = 0*scale
                oldPt = pts[1][1]
                move(vel)
                
                # vel = pts[1][1]-oldPt
                #oldPt = pts[1][1]

                # if pts[1][0]<500:
                #   move(vel,vel)

            # show the frame and record if the user presses a key
            cv2.imshow("cframe", cframe)
            key = cv2.waitKey(1) & 0xFF

            # handle if the 'i' key is pressed, then go into ROI
            # selection mode
            if key == ord("i") and len(roiPts) < 4:
                # indicate that we are in input mode and clone the
                # frame
                inputMode = True
                orig = cframe.copy()

                # keep looping until 4 reference ROI points have
                # been selected; press any key to exit ROI selction
                # mode once 4 points have been selected
                while len(roiPts) < 4:
                    cv2.imshow("cframe", cframe)
                    cv2.waitKey(0)

                # determine the top-left and bottom-right points
                roiPts = np.array(roiPts)
                s = roiPts.sum(axis = 1)
                tl = roiPts[np.argmin(s)]
                br = roiPts[np.argmax(s)]


                # grab the ROI for the bounding box and convert it
                # to the HSV color space
                roi = orig[tl[1]:br[1], tl[0]:br[0]]
                roi = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
                #roi = cv2.cvtColor(roi, cv2.COLOR_BGR2LAB)

                # compute a HSV histogram for the ROI and store the
                # bounding box
                roiHist = cv2.calcHist([roi], [0], None, [16], [0, 180])
                roiHist = cv2.normalize(roiHist, roiHist, 0, 255, cv2.NORM_MINMAX)
                roiBox = (tl[0], tl[1], br[0], br[1])

            # if the 'q' key is pressed, stop the loop
            elif key == ord("q"):
                break

        # cleanup the camera and close any open windows
        camera.release()
        cv2.destroyAllWindows()






class Interface(tk.Frame):

    def __init__ (self,master=None):
        tk.Frame.__init__(self, master)

        global frame
        self.master.rowconfigure(0,weight=1)
        self.master.columnconfigure(0,weight=1)
        frame = tk.Canvas(master, width=width, height=height,background='black')
        frame.pack()



        centerline=frame.create_line(width/2,0,width/2,height,fill='white')
        
        '''creates instruction text'''
        self.title=frame.create_text(width/2,height/2,text='Press Enter to Start a Round',font= 'Times 20', fill='yellow')
        self.text2=frame.create_text(width/4,50,text='Press W/S to move up/down',fill='yellow',font= 'Times 20')
        self.text3=frame.create_text(3*width/4,50,text='Press Up/Down to move up/down',fill='yellow',font= 'Times 20')
        
        self.quitbutton=frame.create_text(width*.02,height*.98,text='QUIT',fill='red',font= 'Times 20')
        self.resetbutton=frame.create_text(width*.98,height*.98,text='RESET',fill='red',font= 'Times 20')

        '''defines boundries'''
        self.leftbound=0
        self.rightbound=windowsize        
        self.topbound=0
        self.bottombound=windowsize

        '''creates score to be displayed'''
        self.scoretext_l=frame.create_text(width*.25,20,text=str(0),fill='white',font= 'Times 20')
        self.scoretext_r=frame.create_text(width*.75,20,text=str(0),fill='white',font= 'Times 20')

        '''creates scales'''
        self.ballspeed=tk.Scale(master,from_=1, to=3, label='Ball Speed', resolution=0.5, orient= tk.HORIZONTAL)
        self.ballspeed.pack(side=tk.LEFT)
        self.paddlespeed=tk.Scale(master,from_=1, to=15,label='Paddle Speed', orient=tk.HORIZONTAL)
        self.paddlespeed.set(4)
        self.paddlespeed.pack(side=tk.LEFT)

        '''create checkbuttons'''
        self.gravityvar=tk.IntVar() #gravity
        self.gravity=tk.Checkbutton(master,text='Gravity', variable=self.gravityvar, \
            onvalue=1,offvalue=0)
        self.gravity.pack(side=tk.LEFT)

        self.breakoutvar=tk.IntVar()
        self.breakoutwalls=tk.Checkbutton(master,text='Breakout',variable=self.breakoutvar, \
            onvalue=1,offvalue=0)
        self.breakoutwalls.pack(side=tk.LEFT)

        self.portalsvar=tk.IntVar()
        self.portals=tk.Checkbutton(master,text='Portals',variable=self.portalsvar, \
            onvalue=1,offvalue=0)
        self.portals.pack(side=tk.LEFT)

        self.creaturevar=tk.IntVar()
        self.creature=tk.Checkbutton(master,text='Monster',variable=self.creaturevar, \
            onvalue=1,offvalue=0)
        self.creature.pack(side=tk.LEFT)

        self.chaoscloudvar=tk.IntVar()
        self.chaoscloud=tk.Checkbutton(master,text='Chaos Cloud',variable=self.chaoscloudvar, \
            onvalue=1,offvalue=0)
        self.chaoscloud.pack(side=tk.LEFT)



        


        # self.ballspeedvar=self.ballspeed.get()
        # self.paddlespeedvar=self.paddlespeed.get()


    def ClearScore(self):
        frame.delete(self.scoretext_l)
        frame.delete(self.scoretext_r)

    def UpdateScore(self,score):
        self.scoretext_l=frame.create_text(width*.25,20,text=str(score[0]),fill='white')
        self.scoretext_r=frame.create_text(width*.75,20,text=str(score[1]),fill='white')


class Monster(object):

    def __init__(self):

    	self.ismonster=gui.creaturevar.get()
        xsize=40
        ysize=75

        self.ballspeedvar=gui.ballspeed.get()
        self.velocity = (self.ballspeedvar*random.choice((-1,1)),self.ballspeedvar*random.choice((-1,1)))

        self.centerx = width/2
        self.centery = height/4

        self.x1=self.centerx-xsize
        self.x2=self.centerx+xsize*.9
        self.y1=self.centery-ysize
        self.y2=self.centery+ysize*.7

        
      
        self.photo = tk.PhotoImage(file='monster.gif')

        self.image = frame.create_image(width/2,height/4,image=self.photo)


      
        

    def move(self):
        """
        Increment ball position, assuming no collisions.
        """
       
        frame.move(self.image,self.velocity[0],self.velocity[1])
        
        self.x1=(self.x1+self.velocity[0])
        self.x2=(self.x2+self.velocity[0])
        self.centerx+=self.velocity[0]

        self.y1=(self.y1+self.velocity[1])
        self.y2=(self.y2+self.velocity[1])
        self.centery+=self.velocity[1]

       

class Paddle(object):

    def __init__(self,name,parameter=0):

        self.name=name
        self.centery = height/2
        self.speed = 0
        xsize=25
        ysize=62

        if(self.name=='Left'):
            frame.bind("<KeyPress-w>", lambda _: self.setpaddlevelocity(1))
            frame.bind("<KeyRelease-w>",lambda _: self.setpaddlevelocity(0))
            frame.bind("<KeyPress-s>", lambda _: self.setpaddlevelocity(-1))
            frame.bind("<KeyRelease-s>",lambda _: self.setpaddlevelocity(0))
            self.centerx = width*.01
            if parameter==0:
                self.photo = tk.PhotoImage(file='benitoface.gif')
                self.paddle = frame.create_image(self.centerx,self.centery,image=self.photo)
            else:
                self.photo = tk.PhotoImage(file='carlos.gif')
                self.paddle = frame.create_image(self.centerx,self.centery,image=self.photo)
                xsize=8
                ysize=85

        else:
            frame.bind("<KeyPress-Down>", lambda _:self.setpaddlevelocity(-1))
            frame.bind("<KeyPress-Up>", lambda _:self.setpaddlevelocity(1)) #needed to use tab and shift. 
            frame.bind("<KeyRelease-Down>",lambda _: self.setpaddlevelocity(0))
            frame.bind("<KeyRelease-Up>", lambda _:self.setpaddlevelocity(0)) #needed to use tab and shift. 
            self.centerx = width * .99
            if parameter==0:
                self.photo = tk.PhotoImage(file='luisface.gif')
                self.paddle = frame.create_image(self.centerx,self.centery,image=self.photo)
            else:
                self.photo = tk.PhotoImage(file='jackface.gif')
                self.paddle = frame.create_image(self.centerx,self.centery,image=self.photo)
                xsize=35
                ysize=80
        frame.focus_set()

        
        'create a paddle with a given initial condition'
        

        self.x1=self.centerx-xsize
        self.x2=self.centerx+xsize*.9
        self.y1=self.centery-ysize*.7
        self.y2=self.centery+ysize*.7

        
   

        self.liney1=frame.create_line(self.x1,self.y1,self.x1,self.y2,fill='green')
        self.liney2=frame.create_line(self.x2,self.y1,self.x2,self.y2,fill='green')

        self.linex1=frame.create_line(self.x1,self.y1,self.x2,self.y1,fill='green')
        self.linex2=frame.create_line(self.x1,self.y2,self.x2,self.y2,fill='green')


       

        frame.bind("<Return>",self.cleartitle)
    print 'gui on'

    
        

    def setpaddlevelocity(self,velocity):
    	
        self.paddlespeedvar=gui.paddlespeed.get()
        self.speed=velocity*(self.paddlespeedvar)
       
        
    def movepaddle(self,speed):


        if(speed<0):
            # print self.y2, height

            if (self.y2<=height):
             
                frame.move(self.paddle,0,-speed)
                self.y1-=self.speed
                self.y2-=self.speed
                # print 'moving'
        
        if(speed>0):
            
            if (self.y1>=0):
                frame.move(self.paddle,0,-speed)
                self.y1-=self.speed
                self.y2-=self.speed
      
                


        frame.delete(self.liney1)
        frame.delete(self.liney2)
        frame.delete(self.linex1)
        frame.delete(self.linex2)

        self.liney1=frame.create_line(self.x1,self.y1,self.x1,self.y2,fill='green')
        self.liney2=frame.create_line(self.x2,self.y1,self.x2,self.y2,fill='green')

        self.linex1=frame.create_line(self.x1,self.y1,self.x2,self.y1,fill='green')
        self.linex2=frame.create_line(self.x1,self.y2,self.x2,self.y2,fill='green')

        		                                    
    def cleartitle(self,event):
         
        frame.delete(gui.title)
        frame.delete(gui.text2)
        frame.delete(gui.text3)
        pong.start=1
        pong.startround()


class Portal(object):
    def __init__(self,centerx,centery):
        
        xsize = 40
        ysize = 40
        self.centerx = centerx
        self.centery = centery

        self.x1=centerx-xsize
        self.x2=centerx+xsize

        self.y1=centery-ysize
        self.y2=centery+ysize

    
    
        self.photo = tk.PhotoImage(file='portal.gif')        
        self.image=frame.create_image(self.centerx, self.centery, image = self.photo)

        

class Cloud(object):
    def __init__(self,centerx,centery):

    	self.iscloud=gui.chaoscloudvar.get()
        
        xsize = 85
        ysize = 30
        self.centerx = centerx
        self.centery = centery

        self.x1=centerx-xsize
        self.x2=centerx+xsize*.95

        self.y1=centery-ysize
        self.y2=centery+ysize*.9

    
    
        self.photo = tk.PhotoImage(file='jokercloud.gif')        
        self.image=frame.create_image(self.centerx, self.centery, image = self.photo)

           
    
class Ball(object):
    def __init__(self):
        """
        Create a pong ball with the given inital position. 
        """

        self.isgravity=gui.gravityvar.get()
        self.theta=random.randint(0,360)*3.14159/180
        self.force=0
        self.radius=0
        self.fieldpoint=(random.randint(0,width), random.randint(0,height))
    

        self.centerx = width/2
        self.centery = height/2

        xsize=15
        ysize=20

        self.radius= sqrt(xsize^2 + ysize^2)

        self.x1=self.centerx-xsize
        self.x2=self.centerx+xsize*.9
        self.y1=self.centery-ysize*.7
        self.y2=self.centery+ysize*.7

        self.ballspeedvar=gui.ballspeed.get()
        self.velocity = (self.ballspeedvar*random.choice((-3,3)),self.ballspeedvar*random.choice((-3,3)))
        
        self.photo = tk.PhotoImage(file='ball.gif')
        self.image = frame.create_image(self.centerx,self.centery,image=self.photo)

        
       
        
 
    def reflect(self,parameter):
        """
        Alter the ball's velocity for a perfectly elastic
        collision with a surface defined by the unit normal surface.
        
        surface: a tuple of floats
        """
        if parameter==1:
            self.velocity=(self.velocity[0],-self.velocity[1])
        if parameter==0:
            self.velocity=(-self.velocity[0],self.velocity[1])

    def translate(self,deltax,deltay):
        frame.move(self.image,deltax,deltay)
        self.x1+=deltax
        self.x2+=deltax
        self.y1+=deltay
        self.y2+=deltay
        self.centerx+=deltax
        self.centery+=deltay


    def move(self,portalstate=0,monsterstate=0):
        """
        Increment ball position, assuming no collisions.
        """
        
        frame.move(self.image,self.velocity[0],self.velocity[1])

        self.x1 += self.velocity[0]
        self.x2 += self.velocity[0]
        self.y1 += self.velocity[1]
        self.y2 += self.velocity[1]

        self.centerx += self.velocity[0]
        self.centery += self.velocity[1]

        if portalstate==1:
         
            deltax=width/2
            deltay=height/2

        if portalstate==2:
            deltax=-width/2
            deltay=height/3
           
        if portalstate==3:
            deltax=width/4
            deltay=-height/1.5

        if portalstate==4:
            deltax=-width/2
            deltay=-height/4
           
        if portalstate==1 or portalstate==2 or portalstate==3 or portalstate==4:
            self.translate(deltax,deltay)
            self.reflect(parameter=random.choice((0,1)))

        if monsterstate==1:
            frame.move(self.image,2*self.velocity[0],self.velocity[1])

       
        
class Obstacle(object):

    def __init__(self,x1,counter):
        """
        Create a wall element
        """

        sizex = 50
        sizey = 75
        self.x1 = x1
        self.x2 = self.x1+sizex
        self.centerx=(self.x1+self.x2)/2-5

        self.y1 = counter*60+45
        self.y2 = self.y1+sizey
      	self.centery = (self.y1 + self.y2)/2

        self.photo = tk.PhotoImage(file='joseface.gif')
        
        self.image = frame.create_image(self.centerx,self.centery,image=self.photo)
        self.state=1
        # self.image= frame.create_rectangle(corner1,corner1,corner2,corner2,fill="blue")
       

class Pong(object):
    def __init__(self):
        self.roundcount=0
        self.start=0
        self.mousex=0
        self.mousey=0
        self.ball = Ball()
        self.paddle1=Paddle('Left',parameter=1)
        self.paddle2=Paddle('Right',parameter=1)

        self.cloud = Cloud (width*.3,height*.3)
        self.createWall()
        self.monster=Monster()
        self.createPortals()

        self.score=[0,0]
        self.bouncenum=0
        self.state=0



        frame.bind('<Button-1>',self.onmouse)
        motion = MotionCap()
        motion.main()

    def createPortals(self):
        self.portal1=Portal(width*.125,40)
        self.portal2=Portal(width*.875,40)
        self.portal3=Portal(width*.125,height-35)
        self.portal4=Portal(width*.875,height-35)
    def deletePortals(self):
    	frame.delete(self.portal1.image)
    	frame.delete(self.portal2.image)
    	frame.delete(self.portal3.image)
    	frame.delete(self.portal4.image)

    def portalcheck(self,ball,portal):
        if (self.ball.x1>=portal.x1) and (self.ball.x2<=portal.x2) and (self.ball.y1>=portal.y1) and (self.ball.y2<=portal.y2):
                #ball.move(portalstate=1)
            if portal.centerx==width*.125 and portal.centery==40:
                ball.move(portalstate=1)          

            if portal.centerx==width*.875 and portal.centery==40:
                ball.move(portalstate=2)
                
            if portal.centerx==width*.125 and portal.centery==height-35:
                ball.move(portalstate=3)

            if portal.centerx==width*.875 and portal.centery==height-35:
                ball.move(portalstate=4)
                
    		   
    def deleteWall(self):
        frame.delete(self.obstacle1.image)
        frame.delete(self.obstacle3.image)
        frame.delete(self.obstacle5.image)
        frame.delete(self.obstacle7.image)
        frame.delete(self.obstacle9.image)
        
    def deleteLines(self,obstacle):
        frame.delete(obstacle.linex1)
        frame.delete(obstacle.liney1)
        frame.delete(obstacle.linex2)
        frame.delete(obstacle.liney2)


    def createWall(self):
        
        x1=random.randrange(width*.25,width*.75)   

        self.obstacle1=Obstacle(x1,0)
        self.obstacle3=Obstacle(x1,2)
        self.obstacle5=Obstacle(x1,4)
        self.obstacle7=Obstacle(x1,6)
        self.obstacle9=Obstacle(x1,8)

    def onmouse(self,event):
        self.mousex=(event.x)
        self.mousey=(event.y)

        if(self.mousex>=0 and self.mousex<=width*.04 and self.mousey>=height*.97):
            root.quit()
            
        elif(self.mousex>=.96*width and self.mousex<=width and self.mousey>=height*.97):
            self.resetscore(1)


    def newcollisioncheck(self,object1,obstacle,name='paddle'):
        
        if object1.velocity[0]<0:
            distancex=abs(object1.centerx-obstacle.x2)
        else:
            distancex=abs(object1.centerx-obstacle.x1)

        theta=atan(object1.velocity[1]/object1.velocity[0]+.0001)*3.14159/180

        distance=distancex/abs(cos(theta))

        if distance < object1.radius*.97 and name !='jose' and object1.centery<=obstacle.y2 and object1.centery>=obstacle.y1:
            object1.reflect(0)
            self.bouncenum+=1

        if distance < object1.radius*.97 and name =='jose'and object1.centery<=obstacle.y2 and object1.centery>=obstacle.y1:
            if obstacle.state==1:
                frame.delete(obstacle.image)
                obstacle.state=0
                object1.reflect(0)

        
        if name == 'cloud':
            distancex=abs(object1.centerx-obstacle.centerx)
            distancey=abs(object1.centery-obstacle.centery) 
            distance=sqrt(distancex**2+distancey**2) 

            if (distance < object1.radius):
                object1.velocity=(object1.velocity[0]*random.choice((1,-1)), object1.velocity[1]*random.choice((1,-1)))          
            

    def resetscore(self,default=0):
        if default==1:          
            
            gui.ClearScore()
            self.score=[0,0]
            gui.UpdateScore([0,0])
            self.start=0
            

    def resetobjects(self):

        gui.ClearScore()
        gui.UpdateScore((self.score[0],self.score[1]))
        self.roundcount+=1

        self.start=0
        self.bouncenum=0

        frame.delete(self.ball.image)
        self.ball = Ball()

        frame.delete(self.paddle1.paddle)
        self.deleteLines(self.paddle1)

        frame.delete(self.paddle2.paddle)
        self.deleteLines(self.paddle2)

        self.paddle1=Paddle('Left',parameter=(self.roundcount%2))
        self.paddle2=Paddle('Right',parameter=(self.roundcount%2))
        
        if(gui.breakoutvar.get()==1):
            self.deleteWall()
            self.createWall()

        if(gui.portalsvar.get()==1):
            self.deletePortals()
            self.createPortals()

        if(gui.chaoscloudvar.get()==1):
            frame.delete(self.cloud.image)
            self.cloud=Cloud(width*.5,height*.5)
        
        if(gui.creaturevar.get()==1):
            frame.delete(self.monster.image)
            self.monster=Monster()
       

        
    def startround(self):
       
        counter=0 #state for ball speed factor
        counter2=0 #state for breakout
        counter3=0 #state for monster
        counter4=0 #state for gravity
        counter5=0 #state for cloud

        while(True):

            while True:

            	self.paddle1.movepaddle(self.paddle1.speed)
                self.paddle2.movepaddle(self.paddle2.speed)
                
                
                y11=self.paddle1.y1
                y21=self.paddle1.y2

                y12=self.paddle2.y1
                y22=self.paddle2.y2

                self.newcollisioncheck(self.ball,self.paddle1)
                self.newcollisioncheck(self.ball,self.paddle2)

                if(gui.breakoutvar.get() == 1):
                    if(counter2==0):
                        self.createWall()
                        counter2+=1
                    self.newcollisioncheck(self.ball,self.obstacle1,name='jose')
                    self.newcollisioncheck(self.ball,self.obstacle3,name='jose')
                    self.newcollisioncheck(self.ball,self.obstacle5,name='jose')
                    self.newcollisioncheck(self.ball,self.obstacle7,name='jose')
                    self.newcollisioncheck(self.ball,self.obstacle9,name='jose')

                else:
                    self.deleteWall()

                
                self.ball.move()

            	time.sleep(.01)
                frame.update()

                

                if(self.bouncenum%2==0) and (self.bouncenum != 0) and self.state==1:
                	counter=(counter+1)%5

                   	self.ball.velocity=(self.ball.velocity[0]*1.4,self.ball.velocity[1]*1.4)
                   	self.state=0
                       



                '''collison possibilites for monster'''
                
                if(gui.creaturevar.get() == 1):
                    if counter3==0:
                        self.monster=Monster()
                        counter3+=1
                    self.newcollisioncheck(self.ball,self.monster)
                    self.monster.move()
                  
                    if(self.monster.x2>=width) or self.monster.x1<=0:
                        self.monster.velocity=(-self.monster.velocity[0],self.monster.velocity[1])
                    
             	    if(self.monster.y2>=height) or self.monster.y1<=0:
                        self.monster.velocity=(self.monster.velocity[0],-self.monster.velocity[1])
                                   
                    if((self.paddle1.y1 >= self.monster.y1) and (self.paddle1.y2<=self.monster.y2) and (self.monster.x1<=30)):                               
                        self.monster.velocity=(-self.monster.velocity[0],self.monster.velocity[1])
                        
                    
                    if((self.paddle2.y1 >= self.monster.y1) and (self.paddle2.y2<=self.monster.y2) and (self.monster.x2>=480)):                
                        self.monster.velocity=(-self.monster.velocity[0],self.monster.velocity[1])
                       

                   
                else:
                    frame.delete(self.monster.image)
                    

                '''collison possibilites for ball'''
                if(self.ball.y2 >= height) or (self.ball.y1 <= 0):
                	self.ball.reflect(1)

                if(gui.gravityvar.get() == 1): 
                    
                    #if checkbox is clicked
                    #calculates distance between ball and chosen gravity point
                    gravity_distance=sqrt((self.ball.centerx-self.ball.fieldpoint[0])**2 + (self.ball.centery-self.ball.fieldpoint[1])**2)
                    #calculates force (if you increase the numerator, field is stronger)
                    self.ball.force=1000/(gravity_distance**2)
                    #calculates new xvel and yvel taking force into account
                    self.ball.velocity = (self.ball.velocity[0] + self.ball.force*(cos(self.ball.theta)),self.ball.velocity[1] + self.ball.force*(sin(self.ball.theta)))
                    xvel=round(self.ball.velocity[0],2)
                    yvel=round(self.ball.velocity[1],2)
                    self.ball.velocity=(xvel,yvel)



               	'''portal collision'''
                
               	if(gui.portalsvar.get() == 1):
                    if counter4==0:
                        self.createPortals()
                        counter4+=1

                    self.portalcheck(self.ball,self.portal1)
                    self.portalcheck(self.ball,self.portal2)
                    self.portalcheck(self.ball,self.portal3)
                    self.portalcheck(self.ball,self.portal4)

             	else:
             		self.deletePortals()


             	'''cloud collisions'''

             	if(gui.chaoscloudvar.get() == 1):
                    if counter5==0:
                        self.cloud=Cloud(width*.3,height*.3)
                        counter5+=1
             		self.newcollisioncheck(self.ball,self.cloud, name ='cloud')

                else:
                    frame.delete(self.cloud.image)
                    



           
            	'''scoring'''       
            	if(self.ball.x1>=width):
                	self.score[0]+=1
                	print "scored right"
                	self.resetobjects()
                        
            	if(self.ball.x2<=0):
                	self.score[1]+=1
                	print "Scored left"
                	self.resetobjects()


            	break
    

global root,gui
root=tk.Tk()




gui=Interface(master=root)
gui.master.title('Pong')


pong=Pong()

# motion.main()

gui.mainloop()
gui.destroy()